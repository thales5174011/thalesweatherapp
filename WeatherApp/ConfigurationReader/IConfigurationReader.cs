﻿namespace WeatherApp.ConfigurationReader
{
    public interface IConfigurationReader
    {
        List<string> GetCities();
        int GetFetchInterval();
        string? GetAPIKey();
    }
}

