﻿using Microsoft.Extensions.Configuration;

namespace WeatherApp.ConfigurationReader
{
    public class ConfigurationReader : IConfigurationReader
    {
        private IConfigurationRoot? _configurationRoot;

        public ConfigurationReader()
        {
            var builder = new ConfigurationBuilder();

            // Set the base path
            builder.SetBasePath(Directory.GetCurrentDirectory());

            // Add the appsettings.json file
            builder.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            // Build the configuration
            _configurationRoot = builder.Build();
        }

        public string? GetAPIKey()
        {
            var apiKey = _configurationRoot?["WeatherAppConfig:ApiKey"];
            return apiKey;
        }

        public List<string> GetCities()
        {
            var section = _configurationRoot?.GetSection("WeatherAppConfig:Countries");
            var children = section?.GetChildren();

            List<string> cities = new List<string>();
            
            if (children != null)
            {
                var nullableCities = children.Select(section => section.Value).ToList();

                cities = nullableCities
                .Where(s => s != null) // Filter out null values
                .Select(s => s!)       // Cast string? to string since we know they are not null
                .ToList();
            }

            return cities;
        }

        public int GetFetchInterval()
        {
            var frequency = _configurationRoot?["WeatherAppConfig:FetchFrequency"];
            if (frequency == null)
                return 60; // default to 60 mintues
            else
                return int.Parse(frequency);
        }
    }
}

