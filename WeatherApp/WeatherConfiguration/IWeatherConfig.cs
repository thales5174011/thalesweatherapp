﻿namespace WeatherApp.WeatherConfig
{
    public interface IWeatherConfig
    {
        int UpdateInterval { get; }
        int NumberOfCities { get; }
        IEnumerable<string> Cities { get; }
    }
}

