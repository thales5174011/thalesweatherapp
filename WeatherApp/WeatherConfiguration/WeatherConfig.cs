﻿namespace WeatherApp.WeatherConfig
{
    public class WeatherConfig : IWeatherConfig
    {
        public WeatherConfig(int updateInterval, int numberOfCities, IEnumerable<string> cities)
        {
            UpdateInterval = updateInterval;
            NumberOfCities = numberOfCities;
            Cities = cities;
        }

        public int UpdateInterval { get; }
        public int NumberOfCities { get; }
        public IEnumerable<string> Cities { get; }
    }
}

