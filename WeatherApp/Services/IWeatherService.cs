namespace WeatherApp.Services
{
    // interface for loose coupling; define weather api contract
	public interface IWeatherService
	{
        // Retrieves weather data for a given city
        Task<string?> GetWeatherData(string city);
    }
}

