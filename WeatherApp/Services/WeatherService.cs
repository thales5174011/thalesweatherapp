namespace WeatherApp.Services
{
    // Concrete implementation of network calls to weather api
	public class WeatherService : IWeatherService
    {
        private readonly string? _apiKey;
        private const string baseUrl = "https://api.weatherapi.com/v1/history.json";

        private static readonly HttpClient _client = new HttpClient();

        public WeatherService(string? apiKey)
		{
            _apiKey = apiKey;
        }

        public async Task<string?> GetWeatherData(string city)
        {
            // figure out end date based on current date
            string currentDate = DateTime.Now.ToString("yyyy-MM-dd");
            string endDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");

            // Query parameters
            var queryParams = new System.Collections.Specialized.NameValueCollection
            {
                { "q", city },
                { "dt", currentDate },
                { "end_dt", endDate },
                { "key", _apiKey}
            };

            // Append query parameters to the base URL
            var uriBuilder = new UriBuilder(baseUrl);
            uriBuilder.Query = string.Join("&", Array.ConvertAll(queryParams.AllKeys, key => $"{Uri.EscapeDataString(key)}={Uri.EscapeDataString(queryParams[key])}"));

            var response = await _client.GetAsync(uriBuilder.Uri);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();

                return responseContent;
            }
            else
            {
                return null;
            }
        }
    }
}

