﻿namespace WeatherApp.Services
{
	public class WeatherWriter
	{
        // Writes weather data to a JSON file
        public async Task WriteData(IEnumerable<string> cities, IEnumerable<string> weatherData)
        {
            // Ensure matching numbers of weather data and cities
            if (weatherData.Count() != cities.Count())
            {
                throw new ArgumentException("Number of weather data items must match the number of cities.");
            }

            // Write each city's weather data to a separate file
            foreach (var (weatherItem, city) in weatherData.Zip(cities))
            {
                await WriteWeatherDataToFile(city, weatherItem);
            }
        }

        // Asynchronously writes a single city's weather data to a JSON file
        private async Task WriteWeatherDataToFile(string city, string weatherData)
        {
            string filePath = city + ".json";

            try
            {
                // Write JSON string to file
                await File.WriteAllTextAsync(filePath, weatherData);

                Console.WriteLine($"JSON data has been written to {filePath}.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error writing JSON data to {filePath}: {ex.Message}");
            }
        }
    }
}

