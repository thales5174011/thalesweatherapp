using WeatherApp.WeatherConfig;
using WeatherApp.Services;

public class Program
{
    // Entry point
    public static void Main(string[] args)
    {
        // Read configuration
        WeatherApp.ConfigurationReader.IConfigurationReader config = new WeatherApp.ConfigurationReader.ConfigurationReader();
        WeatherConfig weatherConfig = new WeatherConfig(config.GetFetchInterval(), config.GetCities().Count, config.GetCities());

        // Initialize services
        var weatherService = new WeatherApp.Services.WeatherService(config.GetAPIKey());
        var weatherWriter = new WeatherWriter();

        // Start background service for updates
        RunAsync(weatherConfig, weatherService, weatherWriter).Wait();
    }

    private static async Task RunAsync(IWeatherConfig config, IWeatherService weatherService, WeatherWriter weatherWriter)
    {
        // infinite loop running periodically to fetch data
        // In production environment, this can be replaced with a serverless function
        // (like Azure Function) which will be triggered periodically.
        while (true)
        {
            var weatherData = await GetWeatherData(config, weatherService);

            await weatherWriter.WriteData(config.Cities, weatherData);

            await Task.Delay(config.UpdateInterval * 1000 * 60); // UpdateInterval is in minutes
        }
    }

    private static async Task<IEnumerable<string>> GetWeatherData(IWeatherConfig config, IWeatherService weatherService)
    {
        // trigger parallel calls to fetch datum for multiple locations
        var tasks = config.Cities.Select(city => weatherService.GetWeatherData(city));
        var weatherData = await Task.WhenAll(tasks);

        string[] weatherDataFiltered = weatherData
            .Where(s => s != null) // Filter out null values
            .Select(s => s!)       // Cast string? to string since we know they are not null
            .ToArray();            // Convert IEnumerable<string> to string[]

        return weatherDataFiltered;
    }
}