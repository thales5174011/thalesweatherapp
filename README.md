# Weather App Configuration

The project requires an `appsettings.json` file with the following configuration:

```json
{
  "WeatherAppConfig": {
    "ApiKey": "your-api-key",
    "FetchFrequency": 15,
    "Countries": [
      "Singapore",
      "Hanoi"
    ]
  }
}
```

ApiKey: Your API key for accessing weather data.
FetchFrequency: The frequency at which weather data should be fetched, specified in minutes. The default is 60 minutes.
Countries: A list of countries for which weather data should be fetched.

## Note

Ensure to replace "your-api-key" with your actual API key.
The FetchFrequency is specified in minutes.

## PS
Due to time constraints, integration of front-end related changes and unit tests has not been completed yet.